---
title: "Nóminas"
draft: false
weight: 2
chapter: true
pre: ""
---

| Fecha | Descipción | Bruto | Líquido |
| ------------- | ------------- | ----- | ----- |
| 30/11/2021 | Mensual - 1 Noviembre 2021 a 30 Noviembre 2021 | 1.600,16 | 1.434,16 |
| 31/10/2021 | 	Mensual - 1 Octubre 2021 a 31 Octubre 2021 | 1.600,16 | 1.434,16 |
